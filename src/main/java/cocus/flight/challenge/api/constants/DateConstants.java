package cocus.flight.challenge.api.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DateConstants {

	public static final String DATE_PATTERN = "yyyy/MM/dd";

}
