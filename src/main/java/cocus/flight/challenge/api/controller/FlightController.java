package cocus.flight.challenge.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cocus.flight.challenge.api.payload.request.FlightAverageFilterRequestPayload;
import cocus.flight.challenge.api.payload.response.FlightAverageDataResponsePayload;
import cocus.flight.challenge.domain.entity.FlightAverageData;
import cocus.flight.challenge.domain.usecase.FlightAverageUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/flight")
public class FlightController {

	private final FlightAverageUseCase flightAverageUseCase;

	@GetMapping(value = "/avg")
	public List<FlightAverageDataResponsePayload> fetch(@Valid FlightAverageFilterRequestPayload filter) {
		log.debug("filter {}", filter);

		List<FlightAverageData> flightAverageDataList = flightAverageUseCase.loadAverages(filter.toEntity());

		return flightAverageDataList.stream().map(FlightAverageDataResponsePayload::new).collect(Collectors.toList());
	}

}
