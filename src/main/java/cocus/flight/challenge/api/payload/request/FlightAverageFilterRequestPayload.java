package cocus.flight.challenge.api.payload.request;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import cocus.flight.challenge.api.constants.DateConstants;
import cocus.flight.challenge.domain.entity.filter.AverageFilter;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FlightAverageFilterRequestPayload {

	@NotNull
	private Currency curr;

	@DateTimeFormat(pattern = DateConstants.DATE_PATTERN)
	private LocalDate dateFrom;

	@DateTimeFormat(pattern = DateConstants.DATE_PATTERN)
	private LocalDate dateTo;

	private List<String> dest;

	public AverageFilter toEntity() {
		return AverageFilter.builder().curr(curr).dateFrom(dateFrom).dateTo(dateTo).dest(dest).build();
	}

}
