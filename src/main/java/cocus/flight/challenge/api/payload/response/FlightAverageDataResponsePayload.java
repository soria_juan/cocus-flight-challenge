package cocus.flight.challenge.api.payload.response;

import java.util.Currency;
import java.util.Map;

import cocus.flight.challenge.domain.entity.FlightAverageData;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FlightAverageDataResponsePayload {

	private String airportCode;
	private String airportName;
	private Currency currency;
	private Integer priceAverage;
	private Map<Integer, Integer> bagPrices;

	public FlightAverageDataResponsePayload(FlightAverageData flightAverageData) {
		airportCode = flightAverageData.getAirportCode();
		airportName = flightAverageData.getAirportName();
		currency = flightAverageData.getCurrency();
		priceAverage = flightAverageData.getPriceAverage();
		bagPrices = flightAverageData.getBagPrices();
	}

}
