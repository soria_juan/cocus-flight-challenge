package cocus.flight.challenge.api.payload.response.error;

import javax.validation.ConstraintViolation;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ErrorFieldPayload {

	private String field;
	private String value;
	private String error;

	public ErrorFieldPayload(String field, String value, String error) {
		this.field = field;
		this.value = value;
		this.error = error;
	}

	public ErrorFieldPayload(ConstraintViolation<?> constraintViolation) {
		field = String.format("Bad request param [%s]", constraintViolation.getPropertyPath().toString());
		value = String.valueOf(constraintViolation.getInvalidValue());
		error = String.format("Param %s", constraintViolation.getMessage());
	}

}
