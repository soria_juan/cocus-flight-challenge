package cocus.flight.challenge.api.payload.response.error;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ErrorResponsePayload {

	private String object;
	private List<ErrorFieldPayload> errorFields;
	private String globalError;

	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	private Object objectPayload;

	private ErrorResponsePayload(String object, List<ErrorFieldPayload> errorFields, String globalError,
			Object objectPayload) {
		this.object = object;
		this.errorFields = errorFields;
		this.globalError = globalError;
		this.objectPayload = objectPayload;
	}

	public static ErrorResponsePayload createWithFields(String object, List<ErrorFieldPayload> errorFields) {
		return new ErrorResponsePayload(object, errorFields, null, null);
	}

	public static ErrorResponsePayload createWithFieldsAndGlobal(String object, List<ErrorFieldPayload> errorFields,
			String globalError) {
		return new ErrorResponsePayload(object, errorFields, globalError, null);
	}

	public static ErrorResponsePayload createGlobal(String object, String globalError) {
		return new ErrorResponsePayload(object, null, globalError, null);
	}

	public static ErrorResponsePayload createGlobal(String globalError) {
		return new ErrorResponsePayload(null, null, globalError, null);
	}

	public static ErrorResponsePayload createGlobalWithObjectPayload(String globalError, Object objectPayload) {
		return new ErrorResponsePayload(null, null, globalError, objectPayload);
	}

}
