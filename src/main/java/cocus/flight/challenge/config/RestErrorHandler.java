package cocus.flight.challenge.config;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import cocus.flight.challenge.api.payload.response.error.ErrorFieldPayload;
import cocus.flight.challenge.api.payload.response.error.ErrorResponsePayload;

@ControllerAdvice
public class RestErrorHandler {

	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	public ResponseEntity<Object> handleArgumentValidationError(MethodArgumentNotValidException e) {
		List<ErrorFieldPayload> errorFields = e.getBindingResult().getFieldErrors().stream()
				.map(error -> new ErrorFieldPayload(error.getField(),
						error.getRejectedValue() == null ? null : error.getRejectedValue().toString(),
						error.getDefaultMessage()))
				.collect(Collectors.toList());

		if (errorFields.isEmpty()) {
			ObjectError globalError = e.getBindingResult().getGlobalError();
			ErrorResponsePayload errorPayload = ErrorResponsePayload.createGlobal(e.getBindingResult().getObjectName(),
					globalError.getDefaultMessage());
			return new ResponseEntity<>(errorPayload, HttpStatus.BAD_REQUEST);
		}

		ErrorResponsePayload errorPayload = ErrorResponsePayload.createWithFields(e.getBindingResult().getObjectName(),
				errorFields);
		return new ResponseEntity<>(errorPayload, HttpStatus.BAD_REQUEST);
	}

//	/**
//	 * Occurs when the query parameters do not match a specified enumerator.
//	 *
//	 * @param bindException the bind exception
//	 * @return the http bad request
//	 */
//	@ExceptionHandler(value = BindException.class)
//	public ResponseEntity<Object> handleBindingResultException(BindException bindException) {
//		log.warn(ExceptionUtils.getStackTrace(bindException));
//
//		List<ErrorFieldPayload> errorFields = bindException.getBindingResult().getFieldErrors().stream()
//				.map(error -> new ErrorFieldPayload(error.getField(),
//						error.getRejectedValue() == null ? null : error.getRejectedValue().toString(),
//						FAILED_TO_BIND_VALUE_TO_FIELD_MESSAGE))
//				.collect(Collectors.toList());
//
//		if (errorFields.isEmpty()) {
//			ErrorResponsePayload errorPayload = ErrorResponsePayload
//					.createGlobal(bindException.getBindingResult().getObjectName(), BIND_GLOBAL_FAILURE_MESSAGE);
//			return new ResponseEntity<>(errorPayload, HttpStatus.BAD_REQUEST);
//		}
//
//		ErrorResponsePayload errorPayload = ErrorResponsePayload
//				.createWithFields(bindException.getBindingResult().getObjectName(), errorFields);
//		return new ResponseEntity<>(errorPayload, HttpStatus.BAD_REQUEST);
//	}
//
//	/**
//	 * Handles constraint exceptions on arguments annotated with
//	 * {@link org.springframework.validation.annotation.Validated}.
//	 *
//	 * Will make sure we have a detailed and comprehensive response with errors by
//	 * field if applicable
//	 *
//	 * @param exception
//	 * @return
//	 */
//	@ExceptionHandler(value = ConstraintViolationException.class)
//	public ResponseEntity<Object> handleConstraintValidationError(ConstraintViolationException exception) {
//		return new ResponseEntity<>(
//				exception.getConstraintViolations().stream().map(ErrorFieldPayload::new).collect(Collectors.toList()),
//				HttpStatus.BAD_REQUEST);
//	}
//
//	/**
//	 * Handles DB exceptions that generally represent Unique constraints not being
//	 * respected, so we return a client error.
//	 *
//	 * @param dataIntegrityException
//	 * @return
//	 * @throws Exception
//	 */
//	@ExceptionHandler(value = DataIntegrityViolationException.class)
//	public ResponseEntity<Object> handleDataIntegrityViolationError(
//			DataIntegrityViolationException dataIntegrityException) {
//		log.warn(ExceptionUtils.getStackTrace(dataIntegrityException));
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(DATA_INTEGRITY_VIOLATION_MESSAGE);
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	/**
//	 * Handles exception coming from Spring trying to parse the JSON to our payload
//	 * entities and failing.
//	 *
//	 * @param e
//	 * @return
//	 * @throws Exception
//	 */
//	@ExceptionHandler(value = { HttpMessageNotReadableException.class, IllegalArgumentException.class,
//			MethodArgumentTypeMismatchException.class })
//	public ResponseEntity<Object> handleError(Exception e) {
//		if (e instanceof HttpMessageNotReadableException) {
//			log.warn(ExceptionUtils.getStackTrace(e));
//			// exception thrown by Spring when JSON format does not match the expected
//			// payload
//			return createBadRequest(INVALID_JSON_FORMAT_MESSAGE);
//		}
//		if (e instanceof IllegalArgumentException) {
//			log.info(ExceptionUtils.getStackTrace(e));
//		} else {
//			log.warn(ExceptionUtils.getStackTrace(e));
//		}
//		return createBadRequest(e.getMessage());
//	}
//
//	/**
//	 * Handles all the other exceptions that may occur, returning a server error.
//	 *
//	 * @param e
//	 * @return
//	 * @throws Exception
//	 */
//	@ExceptionHandler
//	public ResponseEntity<Object> handleGeneric(Exception e) throws Exception {
//		log.error(ExceptionUtils.getStackTrace(e));
//		return new ResponseEntity<>(INTERNAL_SERVER_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
//	}
//
//	/**
//	 * Handles exception when user is not authorized to execute a task, returning a
//	 * Unauthorized error.
//	 *
//	 * @param e
//	 * @param response
//	 * @throws IOException
//	 */
//	@ExceptionHandler(value = UserUnauthorizedException.class)
//	public void handleUnauthorizedError(UserUnauthorizedException e, HttpServletResponse response) throws IOException {
//		log.debug(e.getMessage());
//		response.sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
//	}
//
//	/**
//	 * Handles exception when user is not authorized to execute a task, returning a
//	 * Unauthorized error.
//	 *
//	 * @param e
//	 * @param response
//	 * @throws IOException
//	 */
//	@ExceptionHandler(value = UserExpiredException.class)
//	public void handleExpiredUserError(UserExpiredException e, HttpServletResponse response) throws IOException {
//		log.debug(e.getMessage());
//		response.sendError(HttpStatus.FORBIDDEN.value(), e.getMessage());
//	}
//
//	@ExceptionHandler(value = UserDormantException.class)
//	public void handleDormantUserError(UserDormantException e, HttpServletResponse response) throws IOException {
//		log.debug(e.getMessage());
//		response.sendError(HttpStatus.FORBIDDEN.value(), e.getMessage());
//	}
//
//	@ExceptionHandler(value = UserInactiveException.class)
//	public void handleInactiveUserError(UserInactiveException e, HttpServletResponse response) throws IOException {
//		log.debug(e.getMessage());
//		response.sendError(HttpStatus.FORBIDDEN.value(), e.getMessage());
//	}
//
//	@ExceptionHandler(value = UserForbiddenException.class)
//	public ResponseEntity<Object> handleForbiddenError(Exception e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.FORBIDDEN);
//	}
//
//	@ExceptionHandler(value = InvalidRedirectUrlException.class)
//	public ResponseEntity<Object> handleInvalidRedirect(Exception e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.FORBIDDEN);
//	}
//
//	@ExceptionHandler(value = EntityNotFoundException.class)
//	public ResponseEntity<Object> handleNotFoundError(Exception e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
//	}
//
//	@ExceptionHandler(value = ConflictOperationException.class)
//	public ResponseEntity<Object> handleConflictError(ConflictOperationException e) {
//		log.debug(e.getMessage());
//		return new ResponseEntity<>(ErrorResponsePayload.createGlobal(e.getMessage()), HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = PermissionConflictException.class)
//	public ResponseEntity<Object> handlePermissionConflictError(PermissionConflictException e) {
//		log.debug(e.getMessage());
//		List<ErrorFieldPayload> errorFields = new ArrayList<>();
//		if (e.getPermissionConflictFields() != null) {
//			e.getPermissionConflictFields().forEach((permissionConflictEntity, count) -> {
//				errorFields.add(new ErrorFieldPayload(permissionConflictEntity.name(), Long.toString(count),
//						e.buildFieldMessage(permissionConflictEntity)));
//			});
//		}
//		ErrorResponsePayload errorResponsePayload = ErrorResponsePayload
//				.createWithFieldsAndGlobal(PermissionConflict.class.getName(), errorFields, e.getMessage());
//		return new ResponseEntity<>(errorResponsePayload, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = RoleUsersLimitException.class)
//	public ResponseEntity<Object> handleRoleUsersLimit(Exception e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = DuplicateUserException.class)
//	public ResponseEntity<Object> handleDuplicateUser(DuplicateUserException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new UserResponsePayload(e.getDuplicateUser()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = DuplicateTargetTypeException.class)
//	public ResponseEntity<Object> handleDuplicateTargetType(DuplicateTargetTypeException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new TargetTypeResponsePayload(e.getDuplicateTargetType()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//
//	}
//
//	@ExceptionHandler(value = DuplicateApplicationException.class)
//	public ResponseEntity<Object> handleDuplicateApplication(DuplicateApplicationException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new ApplicationResponsePayload(e.getDuplicateApplication()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//
//	}
//
//	@ExceptionHandler(value = DuplicateDataTypeOwnerException.class)
//	public ResponseEntity<Object> handleDuplicateDataTypeOwner(DuplicateDataTypeOwnerException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new DataTypeOwnerResponsePayload(e.getDuplicateDataTypeOwner()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = DuplicatePermissionConflictException.class)
//	public ResponseEntity<Object> handleDuplicatePermissionConflict(DuplicatePermissionConflictException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new PermissionConflictResponsePayload(e.getDuplicatePermissionConflict()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = DuplicateAccessRequestAssessmentException.class)
//	public ResponseEntity<Object> handleDuplicateAccessRequestAssessment(DuplicateAccessRequestAssessmentException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new AccessRequestAssessmentResponsePayload(e.getDuplicateAccessRequestAssessment()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = ClosedUserRoleAccessRequestException.class)
//	public ResponseEntity<Object> handleClosedUserRoleAccessRequest(ClosedUserRoleAccessRequestException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = PendingUserRoleAccessRequestException.class)
//	public ResponseEntity<Object> handlePendingUserRoleAccessRequestException(PendingUserRoleAccessRequestException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = UserAlreadyInGroupException.class)
//	public ResponseEntity<Object> handleUserAlreadyInGroup(Exception e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
//	}
//
//	@ExceptionHandler(value = DuplicateGroupException.class)
//	public ResponseEntity<Object> handleDuplicateGroup(DuplicateGroupException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new GroupResponsePayload(e.getDuplicatedGroup()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = DuplicateRedirectHostException.class)
//	public ResponseEntity<Object> handleDuplicateRedirect(DuplicateRedirectHostException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobalWithObjectPayload(e.getMessage(),
//				new RedirectHostResponsePayload(e.getDuplicateRedirectHost()));
//		return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
//	}
//
//	@ExceptionHandler(value = MissingServletRequestParameterException.class)
//	public ResponseEntity<Object> handleMissingServletRequestParameter(Exception e) {
//		log.warn(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
//	}
//
//	@ExceptionHandler(value = AuthException.class)
//	public ResponseEntity<Object> handleAuthException(AuthException e) {
//		log.debug(e.getMessage());
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(e.getMessage());
//		return new ResponseEntity<>(errorResponse, HttpStatus.UNAUTHORIZED);
//	}
//
//	@NotNull
//	private ResponseEntity<Object> createBadRequest(String message) {
//		ErrorResponsePayload errorResponse = ErrorResponsePayload.createGlobal(message);
//		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
//	}

}
