package cocus.flight.challenge.domain.entity;

import java.util.Currency;
import java.util.Map;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class FlightAverageData {

	private String airportCode;
	private String airportName;
	private Currency currency;
	private Integer priceAverage;
	private Map<Integer, Integer> bagPrices;

}
