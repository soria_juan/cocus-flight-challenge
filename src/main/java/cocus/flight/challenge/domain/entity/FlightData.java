package cocus.flight.challenge.domain.entity;

import java.math.BigDecimal;
import java.util.Map;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FlightData {

	private String flyTo;
	private BigDecimal price;
	private Map<String, BigDecimal> bagsPrice;

}
