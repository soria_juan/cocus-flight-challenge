package cocus.flight.challenge.domain.entity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchFlightData {

	private List<FlightData> flights;

}
