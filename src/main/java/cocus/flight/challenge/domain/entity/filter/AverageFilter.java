package cocus.flight.challenge.domain.entity.filter;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class AverageFilter {

	@NotNull
	private Currency curr;

	private LocalDate dateFrom;
	private LocalDate dateTo;
	private List<String> dest;

}
