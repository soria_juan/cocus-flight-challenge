package cocus.flight.challenge.domain.exception;

import lombok.Getter;

@Getter
public class NetworkException extends RuntimeException {

	private static final long serialVersionUID = -8022212759206663197L;

	private final int code;
	private final String payload;

	public NetworkException(int code, String payload) {
		super(code + " -> " + payload);
		this.code = code;
		this.payload = payload;
	}

}
