package cocus.flight.challenge.domain.usecase;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import cocus.flight.challenge.domain.entity.FlightAverageData;
import cocus.flight.challenge.domain.entity.filter.AverageFilter;

@Component
public class FlightAverageUseCase {

	public List<FlightAverageData> loadAverages(AverageFilter filter) {

		// TODO filter deve ser passado para a proxima camada para ser usado
		
		// TODO implementar logica para devolver objeto preenchido
		return Collections.singletonList(FlightAverageData.builder().build());
	}

}
