package cocus.flight.challenge.network;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

import org.springframework.stereotype.Component;

import cocus.flight.challenge.domain.entity.SearchFlightData;
import cocus.flight.challenge.domain.exception.NetworkException;
import cocus.flight.challenge.network.skypicker.SearchFlightResponse;
import cocus.flight.challenge.network.skypicker.SkyPickerApiClient;
import retrofit2.Response;

@Component
public class FlightInfoNetworkRequester {

	private final SkyPickerApiClient skyPickerApiClient;

	public FlightInfoNetworkRequester(NetworkClientFactory networkClientFactory) {
		this.skyPickerApiClient = networkClientFactory.createSkyPickerApiClient();
	}

	public SearchFlightData searchFlights(List<String> flyFrom, List<String> flyTo, LocalDate dateFrom,
			LocalDate dateTo, Currency curr, List<String> selectAirlines) throws IOException {
		Response<SearchFlightResponse> response = skyPickerApiClient
				.searchFlights(flyFrom, flyTo, dateFrom, dateTo, curr, selectAirlines).execute();

		if (response.isSuccessful()) {
			// return response.body().buildUserSocialProfile();
			return null;
		}

		throw new NetworkException(response.code(), response.message());
	}

}
