package cocus.flight.challenge.network;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import cocus.flight.challenge.network.skypicker.SkyPickerApiClient;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class NetworkClientFactory {

	@Value("${skypicker.api.url:https://api.skypicker.com}")
	private String skyPickerApiUrl;

	public SkyPickerApiClient createSkyPickerApiClient() {
		return createRetrofitInstance(skyPickerApiUrl, createOkHttpInstance()).create(SkyPickerApiClient.class);
	}

	private OkHttpClient.Builder createOkHttpInstance() {
		return new OkHttpClient.Builder();
	}

	private Retrofit createRetrofitInstance(String endpoint, OkHttpClient.Builder okHttpBuilder) {
		return new Retrofit.Builder().baseUrl(endpoint).addConverterFactory(GsonConverterFactory.create())
				.client(okHttpBuilder.build()).build();
	}

}
