package cocus.flight.challenge.network.skypicker;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.SerializedName;

import cocus.flight.challenge.domain.entity.SearchFlightData;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchFlightResponse {

	private List<FlightDataResponse> data;

	public SearchFlightData toEntity() {
		return SearchFlightData.builder()
				// .flights(data.stream().map())
				.build();
	}

}

@Data
@Builder
class FlightDataResponse {

	private String flyTo;
	private BigDecimal price;

	@SerializedName("bags_price")
	private Map<String, BigDecimal> bagsPrice;

}
