package cocus.flight.challenge.network.skypicker;

import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SkyPickerApiClient {

	@GET("/flights?v=3&partner=picky&vehicle_type=aircraft")
	Call<SearchFlightResponse> searchFlights(@Query("fly_from") List<String> flyFrom,
			@Query("fly_to") List<String> flyTo, @Query("date_from") LocalDate dateFrom,
			@Query("date_to") LocalDate dateTo, @Query("curr") Currency curr,
			@Query("select_airlines") List<String> selectAirlines);

}
