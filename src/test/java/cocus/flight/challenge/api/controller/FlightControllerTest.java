package cocus.flight.challenge.api.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import cocus.flight.challenge.api.constants.DateConstants;
import cocus.flight.challenge.domain.entity.filter.AverageFilter;
import cocus.flight.challenge.domain.usecase.FlightAverageUseCase;

@WebMvcTest(FlightController.class)
public class FlightControllerTest {

	private static final String BASE_URI = "/flight";
	private static final String AVG_URI = BASE_URI + "/avg";

	private static final String CURR_PARAM_NAME = "curr";
	private static final String CURR_PARAM_VALUE = "GBP";
	private static final String DATE_FROM_PARAM_NAME = "dateFrom";
	private static final String DATE_FROM_PARAM_VALUE = "2020/01/01";
	private static final String DATE_TO_PARAM_NAME = "dateTo";
	private static final String DATE_TO_PARAM_VALUE = "2020/12/31";
	private static final String DEST_PARAM_NAME = "dest";
	private static final String[] DEST_PARAM_VALUE = new String[] { "LIS", "OPO" };

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private FlightAverageUseCase flightAverageUseCase;

	@Test
	public void testNoFilterParamsBadRequest() throws Exception {
		mockMvc.perform(get(AVG_URI).accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();

		verifyNoInteractions(flightAverageUseCase);
	}

	@Test
	public void testCurrFilterParam() throws Exception {
		AverageFilter filter = AverageFilter.builder().curr(Currency.getInstance(CURR_PARAM_VALUE)).build();
		when(flightAverageUseCase.loadAverages(filter)).thenReturn(Collections.emptyList());

		mockMvc.perform(get(AVG_URI).accept(MediaType.APPLICATION_JSON).queryParam(CURR_PARAM_NAME, CURR_PARAM_VALUE))
				.andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$").exists()).andExpect(jsonPath("$").isArray()).andReturn();

		verify(flightAverageUseCase).loadAverages(filter);
	}

	@Test
	public void testDateFromFilterParam() throws Exception {
		AverageFilter filter = AverageFilter.builder().curr(Currency.getInstance(CURR_PARAM_VALUE))
				.dateFrom(
						LocalDate.parse(DATE_FROM_PARAM_VALUE, DateTimeFormatter.ofPattern(DateConstants.DATE_PATTERN)))
				.build();
		when(flightAverageUseCase.loadAverages(filter)).thenReturn(Collections.emptyList());

		mockMvc.perform(get(AVG_URI).accept(MediaType.APPLICATION_JSON).queryParam(CURR_PARAM_NAME, CURR_PARAM_VALUE)
				.queryParam(DATE_FROM_PARAM_NAME, DATE_FROM_PARAM_VALUE)).andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$").exists()).andExpect(jsonPath("$").isArray()).andReturn();

		verify(flightAverageUseCase).loadAverages(filter);
	}

	@Test
	public void testDateToFilterParam() throws Exception {
		AverageFilter filter = AverageFilter.builder().curr(Currency.getInstance(CURR_PARAM_VALUE))
				.dateTo(LocalDate.parse(DATE_TO_PARAM_VALUE, DateTimeFormatter.ofPattern(DateConstants.DATE_PATTERN)))
				.build();
		when(flightAverageUseCase.loadAverages(filter)).thenReturn(Collections.emptyList());

		mockMvc.perform(get(AVG_URI).accept(MediaType.APPLICATION_JSON).queryParam(CURR_PARAM_NAME, CURR_PARAM_VALUE)
				.queryParam(DATE_TO_PARAM_NAME, DATE_TO_PARAM_VALUE)).andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$").exists()).andExpect(jsonPath("$").isArray()).andReturn();

		verify(flightAverageUseCase).loadAverages(filter);
	}

	@Test
	public void testDestFilterParam() throws Exception {
		AverageFilter filter = AverageFilter.builder().curr(Currency.getInstance(CURR_PARAM_VALUE))
				.dest(Arrays.asList(DEST_PARAM_VALUE)).build();
		when(flightAverageUseCase.loadAverages(filter)).thenReturn(Collections.emptyList());

		mockMvc.perform(get(AVG_URI).accept(MediaType.APPLICATION_JSON).queryParam(CURR_PARAM_NAME, CURR_PARAM_VALUE)
				.queryParam(DEST_PARAM_NAME, DEST_PARAM_VALUE)).andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$").exists()).andExpect(jsonPath("$").isArray()).andReturn();

		verify(flightAverageUseCase).loadAverages(filter);
	}

	@Test
	public void testAllFilterParams() throws Exception {
		AverageFilter filter = AverageFilter.builder().curr(Currency.getInstance(CURR_PARAM_VALUE))
				.dateFrom(
						LocalDate.parse(DATE_FROM_PARAM_VALUE, DateTimeFormatter.ofPattern(DateConstants.DATE_PATTERN)))
				.dateTo(LocalDate.parse(DATE_TO_PARAM_VALUE, DateTimeFormatter.ofPattern(DateConstants.DATE_PATTERN)))
				.dest(Arrays.asList(DEST_PARAM_VALUE)).build();
		when(flightAverageUseCase.loadAverages(filter)).thenReturn(Collections.emptyList());

		mockMvc.perform(get(AVG_URI).accept(MediaType.APPLICATION_JSON).queryParam(CURR_PARAM_NAME, CURR_PARAM_VALUE)
				.queryParam(DATE_FROM_PARAM_NAME, DATE_FROM_PARAM_VALUE)
				.queryParam(DATE_TO_PARAM_NAME, DATE_TO_PARAM_VALUE).queryParam(DEST_PARAM_NAME, DEST_PARAM_VALUE))
				.andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$").exists()).andExpect(jsonPath("$").isArray()).andReturn();

		verify(flightAverageUseCase).loadAverages(filter);
	}

	@Test
	public void testInvalidFilterParamBadRequest() throws Exception {
		mockMvc.perform(get(AVG_URI).accept(MediaType.APPLICATION_JSON).queryParam("invalid", "param"))
				.andExpect(status().isBadRequest()).andReturn();

		verifyNoInteractions(flightAverageUseCase);
	}

}
