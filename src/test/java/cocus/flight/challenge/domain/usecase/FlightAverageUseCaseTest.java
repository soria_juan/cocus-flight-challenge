package cocus.flight.challenge.domain.usecase;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import cocus.flight.challenge.domain.entity.FlightAverageData;
import cocus.flight.challenge.domain.entity.filter.AverageFilter;

@ExtendWith(MockitoExtension.class)
public class FlightAverageUseCaseTest {

	@InjectMocks
	private FlightAverageUseCase flightAverageUseCase;

	@Test
	public void loadAveragesTest() {
		AverageFilter filter = AverageFilter.builder().curr(Currency.getInstance("GBP")).dateFrom(LocalDate.now())
				.dateTo(LocalDate.now()).dest(Arrays.asList("A", "B", "C")).build();

		List<FlightAverageData> averages = flightAverageUseCase.loadAverages(filter);

		assertNotNull(averages);
	}

}
